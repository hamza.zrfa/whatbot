require('dotenv').config()

const fs = require('fs')
//const axios = require('axios')
const inquirer = require('inquirer')
const chalk = require('chalk')

const { Client, LocalAuth } = require('whatsapp-web.js')
const qrcode = require('qrcode-terminal')

//Openai configuration
const { Configuration, OpenAIApi } = require("openai");

const config = new Configuration({
	apiKey: "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",
    //apiKey: "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",
});

const openai = new OpenAIApi(config);

// Check for API keys.
if (!process.env.OPENAI_SECRET_KEY) {
    console.error(chalk.red('MISSING API KEY'), 'Please create an .env file that includes a variable named OPENAI_SECRET_KEY')
    process.exit()
}

// Set up session.
const SESSION_FILE_PATH = './session.json'
let sessionCfg
if (fs.existsSync(SESSION_FILE_PATH)) {
    sessionCfg = require(SESSION_FILE_PATH)
}

// Set up default prompt. credit: https://gist.github.com/mayfer/3c358fe638607531750582f9bad21d78
let defaultPrompt = process.env.DEFAULT_PROMPT ? process.env.DEFAULT_PROMPT : "I am a person who perceives the world without prejudice or bias. Fully neutral and objective, I see reality as it actually is and can easily draw accurate conclusions about advanced topics and human society in general."

// Create array of selected contacts.
let selectedContacts = []

// Instantiate new WhatsApp client.
const client = new Client({ session: sessionCfg, restartOnAuthFail: true, authStrategy: new LocalAuth() })
console.log('Starting WhatsApp client...\n')

// On QR code.
client.on('qr', (qr) => {
    console.clear()
    console.log('\n1. Open WhatsApp on your phone\n2. Tap Menu or Settings and select WhatsApp Web\n3. Point your phone to this screen to capture the code\n')

    // Display QR code.
    qrcode.generate(qr, { small: true })
})

// On authentication.
client.on('authenticated', (session) => {
    console.log('WhatsApp authentication successful.\n')

    /*
    // Set current session and write to file.
    sessionCfg = session
    fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (error) {
        if (error) {
            console.error(chalk.red('SESSION FAILURE'), error)
        }
    })
    */
})

// On auth failure.
client.on('auth_failure', message => {
    console.error(chalk.red('WHATSAPP AUTHENTICATION FAILURE'), message)
})

// On client ready.
client.on('ready', async () => {
    console.log('Whatbot is ready!\n')

    // Get list of current chat instances.
    client.getChats().then((chats) => {
        let contactChoices = []
        // Loop through chats and build choices array.
        chats.forEach((item, index) => {
            if (index <= 5) {
                contactChoices.push({ name: item.name, value: item.id._serialized })
            }
        })
        inquirer
            .prompt([
                {
                    name: 'prompt',
                    message: 'Define your AI personality (press enter for default):',
                    default: defaultPrompt,
                    suffix: '\n'
                },
                {
                    type: 'checkbox',
                    name: 'contacts',
                    message: 'Select contacts:',
                    choices: contactChoices,
                    validate: function (answer) {
                        if (answer.length < 1) {
                            return 'You must choose at least one contact.'
                        }
                        return true
                    },
                },
            ])
            .then(answers => {
                // Set AI prompt.
                defaultPrompt = answers.prompt
                // Set selected contacts array.
                selectedContacts = answers.contacts
                console.log(chalk.greenBright('\nAI activated. Listening for messages...\n'))
            })
            .catch(error => {
                console.error(chalk.red('PROMPT FAILURE'), error)
            })
    })
})

// On message received.
client.on('message', async (message) => {

    // If AI is enabled for this contact.
    if (selectedContacts.includes(message.from)) {

        // Set my name (first name only).
        const myName = "Ante Diop (also known as bou3lem)"

        // Get contact.
        const contact = await message.getContact()

        // Get contact name.
        const contactName = contact.shortName

        // Log message.
        console.log(contactName + ': ' + message.body)
        
        if (true){
            // Get Chat.
            const chat = await message.getChat()

            // Set prompt.
            //let prompt = defaultPrompt + " Below are some of my conversations with my friend " + contactName + '.\n\n'
            let prompt = "This is a conversation. The aim is that you continue the conversation by saying something logical and appropriate to the situation. In this discussion there is Youcef, Basile (also called Yassin) and yourself (you are called Ante diop, also known as Bou3lem). If you answer Youcef, don't hesitate to be mean (try to frustrate him, to make him angry. You have to be bitter with him). On the other hand, with Basile, be nice. Oh yes, and speak with the appropriate language. What I mean is that if you are contacted as a Bou3lem, you should speak like a Moroccan. On the other hand, if you are approached as Ante Diop, you should speak like the scholar Ante Diop. Here is the conversation:";
            //let prompt_analyze = "In the following conversation, your name is Ante Diop (also known as bou3lem). The aim is for you to tell me if you are concerned by the discussion. Answer 0 for no and 1 for yes : ";
            let prompt_analyze = "In the following conversation, your name is Ante Diop (also known as bou3lem). You have to tell me if you are concerned by the discussion. For example, if you are being teased, you must reply. Answer 0 only if you are not included in the discussion or if you are asked to keep quiet. Answer 0 for no and 1 for yes: ";

            // Loop through last 10 messages of history.
            const history = await chat.fetchMessages({ limit: 6 })
            history.forEach(function (item, index) {
                // Get author name
                const name = item.from == message.from ? contactName : 'Me (' + myName + ')'
                // Add to prompt.
                if (!prompt.includes(item.body)) {
                    prompt += name + ': ' + item.body + '\n'
                    prompt_analyze += name + ': ' + item.body + '\n'
                }
            })

            //Does the Ai have to respond?
            //prompt_analyze = "In the following conversation, your name is Ante Diop (also known as bou3lem). The aim is for you to tell me if you are concerned by the discussion. Answer 0 for no and 1 for yes : ";

            console.log("prompt_analyze : " + prompt_analyze);

            const response = await openai.createCompletion({
                model: 'text-davinci-002',
                prompt: prompt_analyze,
                max_tokens: 3000,
                temperature: 0.3,
                top_p: 1.0,
                presence_penalty: 0.0,
                frequency_penalty: 0.0,
            });

            if ((response.data.choices[0].text).includes("1")){
                console.log("AI is responding ; " +response.data.choices[0].text);
                // Finalize prompt.
                prompt += 'Me (' + myName + '):'

                console.log('prompt : ' + prompt);

                // Set typing state.
                chat.sendStateTyping()

                // Query GPT-3 API.
                const runPrompt = async (query) => {
                    const prompt = query;

                    //final response
                    const response = await openai.createCompletion({
                        model: 'text-davinci-002', //3
                        prompt: prompt,
                        max_tokens: 3000,
                        temperature: 0.3,
                        top_p: 1.0,
                        presence_penalty: 0.0,
                        frequency_penalty: 0.0,
                    });
                    message.reply(response.data.choices[0].text);
                };
                
                runPrompt(prompt);
            }else {
                console.log("AI is not responding :" + +response.data.choices[0].text);
            }

        }
    }
})

// Initialize WhatsApp client.
client.initialize()

// Handle graceful shutdown.
process.on('SIGINT', function () {
    process.exit()
})